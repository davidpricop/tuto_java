package com.novelius;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Servlet
 */
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	private String rutaJsp;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		rutaJsp = config.getInitParameter("rutaJsp");
		System.out.println(rutaJsp);

	}
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String  accion = request.getParameter("accion");
		
		if (accion != null) {
			if(accion.equals("login")) {
				setRespuestaControlador(accion).forward(request, response);
			}
			else if (accion.equals("inicio")) {
				setRespuestaControlador("index").forward(request, response);
			}
			
			
		}else {
			setRespuestaControlador("index").forward(request, response);
		}
	}

	public RequestDispatcher setRespuestaControlador(String vista){
		String url = rutaJsp + vista + ".jsp";
		return getServletContext().getRequestDispatcher(url);
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String  accion = request.getParameter("accion");
		
		if (accion != null) {
			if (accion.equals("iniciarSesion")) {
				String usuario = request.getParameter("usuario");
				String contrasena = request.getParameter("contrasena");
				
				//�mbito request => solo se ejecuta una vez
				request.setAttribute("usuario", usuario);
				request.setAttribute("contrasena", contrasena);
				
				//�mbito sessi�n => existira siempre y cuando que la ventana del navegador este abierta o no se haya destruido con una linea de codigo destroy        
				HttpSession sesion = request.getSession();
				sesion.setAttribute("usuario", usuario);
				sesion.setAttribute("contrasena", contrasena);
				
				//�mbito contexto
				ServletContext context = getServletContext();
				context.setAttribute("usuario",usuario);
				context.setAttribute("contrasena",contrasena);

				setRespuestaControlador("postlogin").forward(request, response);
			}
		}else {
			getServletContext().getRequestDispatcher(rutaJsp +  "index.jsp").forward(request, response);
		}
	}

}
